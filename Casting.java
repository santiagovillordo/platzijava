public class Casting {
    public static void main(String[] args) {
        /*

        Usando los dos tipos de casting que aprendiste (implícito y explícito), resuelve los siguientes casteos indicando qué tipo es y si se está calculando estimación o exactitud.

char c = ‘z’; conviertelo a int
int i = 250; conviertelo a long y luego de long a short
double d = 301.067; conviertelo a long
int i = 100; súmale 5000.66 y conviertelo a float
int i = 737; multiplícalo por 100 y conviertelo a byte
double d = 298.638; divídelo entre 25 y conviertelo a long

         */
        char c = 'z';
        int cI = c; // casteo implicito exactitud es una conversión exacta ya que el valor 122 corresponde al código ASCII del carácter 'z'.

        int i = 250;
        long iL = 250L; // casteo implicito  exactitud se realiza un casting implícito de int a long, lo cual es una conversión exacta ya que ambos tipos pueden contener el valor 250.
        short iLS = (short) iL; // casteo explicito estimacion Luego se realiza un casting explícito de long a short, lo cual es una estimación ya que se pierden los bits de mayor peso. En este caso, el valor 250 se mantiene

        double d = 301.067;
        long dL = (long) d; // casteo explicito estimacion Aquí se realiza un casting explícito de double a long. Es una estimación ya que se descartan los decimales. El valor 301.067 se convierte en 301.

        int y = 100; //súmale 5000.66 y conviertelo a float
        float yF = y + 5000.66F; // casteo implicito exactitud  En este caso, se realiza una operación de suma entre int y float, y luego se realiza un casting implícito de int a float. La conversión es exacta ya que el valor 100 se suma al valor 5000.66 y el resultado es 5100.66.

        int x = 737; // multiplícalo por 100 y conviertelo a byte
        byte xB = (byte) (x * 100); // casteo explicito estimacion  En este caso, se realiza una multiplicación de int por int, y luego se realiza un casting explícito de int a byte. Es una estimación ya que se descartan los bits de mayor peso. El resultado de la multiplicación 737 * 100 es 73700, pero como se realiza un casting a byte, el resultado final es -28.

        double p = 298.638; //divídelo entre 25 y conviertelo a long
        long pL = (long) (p / 25); // casteo explicito estimacion En este caso, se realiza una división de double por int, y luego se realiza un casting explícito de double a long. Es una estimación ya que se descartan los decimales. El resultado de la división 298.638 / 25 es 11.94552, pero como se realiza un casting a long, el resultado final es 11.


        System.out.println(c + " " + cI);
        System.out.println(i + " " + iL + " " + iLS);
        System.out.println(d + " " + dL);
        System.out.println(y + " " + yF);
        System.out.println(x + " " + xB);
        System.out.println(p + " " + pL);

    }
}
