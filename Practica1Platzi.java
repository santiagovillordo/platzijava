public class Practica1Platzi {
    public static void main(String[] args) {
        // Nombres de las personas
        String nombre1 = "Juan";
        String nombre2 = "María";
        String nombre3 = "Pedro";
        String nombre4 = "Laura";
        String nombre5 = "Carlos";

        // Edades de las personas
        int edad1 = 25;
        int edad2 = 30;
        int edad3 = 42;
        int edad4 = 49;
        int edad5 = 50;

        // Profesiones de las personas
        String profesion1 = "Ingeniero";
        String profesion2 = "Abogada";
        String profesion3 = "Doctor";
        String profesion4 = "Cocinera";
        String profesion5 = "Empresario";

        // Comidas favoritas de las personas
        String comida1 = "Pizza";
        String comida2 = "Sushi";
        String comida3 = "Hamburguesa";
        String comida4 = "Ensalada";
        String comida5 = "Pastas";

        // Relación con cada miembro de la familia
        String relacion1 = "Hermano";
        String relacion2 = "Hermana";
        String relacion3 = "Tío";
        String relacion4 = "Madre";
        String relacion5 = "Padre";

        // Imprimir los datos de cada persona
        System.out.println("Miembros de la familia:");
        System.out.println("Mi " + relacion1 + " " + nombre1 + " tiene  " + edad1 + " años es  " + profesion1 + " y su comida favorita es : " + comida1);
        System.out.println("Mi " + relacion2 + " " + nombre2 + " tiene  " + edad2 + " años es  " + profesion2 + " y su comida favorita es : " + comida2);
        System.out.println("Mi " + relacion3 + " " + nombre3 + " tiene  " + edad3 + " años es  " + profesion3 + " y su comida favorita es : " + comida3);
        System.out.println("Mi " + relacion4 + " " + nombre4 + " tiene  " + edad4 + " años es  " + profesion4 + " y su comida favorita es : " + comida4);
        System.out.println("Mi " + relacion5 + " " + nombre5 + " tiene  " + edad5 + " años es  " + profesion5 + " y su comida favorita es : " + comida5);


    }


}

